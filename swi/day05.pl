:- use_module(library(pcre)).
:- use_module(library(clpfd)).

input(I) :-
  read_file_to_string("swi/day05.txt", I, []).

parsed_input([""], []).
parsed_input([_, "    " | Rest], List) :-
  parsed_input(Rest, RestOfList),
  List = [nothing | RestOfList].

parsed_input([_, Crate | Rest], List) :-
  string_concat("[", CrateWithBracket, Crate),
  string_concat(CrateStr, "] ", CrateWithBracket),
  parsed_input(Rest, RestOfList),
  List = [CrateStr | RestOfList].

parse_crates(CratesString, CrateList) :-
  re_split("\n", CratesString, CratesStack, []),
  member(CrateStack, CratesStack),
  CrateStack \= "\n",
  re_split(".{4}", CrateStack, CratesWithSpacing, []),
  parsed_input(CratesWithSpacing, CrateList).

remove_empty(List, Res) :-
  member(Res, List),
  Res \= nothing.

remove_all_empty(CrateList, NonEmpty) :-
  transpose(CrateList, List),
  member(Row, List),
  bagof(E, remove_empty(Row, E), NonEmpty).

normalize_crates(CratesString, Crates) :-
  bagof(C, parse_crates(CratesString, C), CrateList),
  bagof(E, remove_all_empty(CrateList, E), Crates).

assoc([_ | Rst], 1, Elem, [Elem | Rst]).
assoc([Fst | Rst], Idx, Elem, NewList) :-
  Idx \= 1,
  NewIdx is Idx - 1,
  assoc(Rst, NewIdx, Elem, Replaced),
  NewList = [Fst | Replaced].

moved(Crates, [0, _, _], Crates) :- !.
moved(Crates, [Many, From, To], NewCrates) :-
  nth1(From, Crates, [FirstFrom | RestFrom]),
  nth1(To, Crates, ToList),
  assoc(Crates, From, RestFrom, Without),
  assoc(Without, To, [FirstFrom | ToList], TempNewCrates),
  NewMany is Many - 1,
  moved(TempNewCrates, [NewMany, From, To], NewCrates).

all_movements([], Crates, Crates) :- !.
all_movements([Many, From, To | Rest], Crates, NewCrates) :-
  moved(Crates, [Many, From, To], TempCrates),
  all_movements(Rest, TempCrates, NewCrates).

make_moves(Result) :-
  input(I),
  re_split("\n.*?\n\n"/m, I, [CratesString, _, Instructions], []),
  normalize_crates(CratesString, Crates),
  re_split("\\d+"/n, Instructions, Inst),
  include(number, Inst, NumbersOnly),
  all_movements(NumbersOnly, Crates, Result).

fst(List, A) :- nth1(1, List, A).
first_part(Res) :-
  make_moves(Result),
  maplist(fst, Result, Res).

moved2(Crates, [Many, From, To], NewCrates) :-
  nth1(From, Crates, FromList),
  nth1(To, Crates, ToList),
  append(BeforeFrom, RestFrom, FromList),
  length(BeforeFrom, Many),
  append(BeforeFrom, ToList, NewToList),
  assoc(Crates, From, RestFrom, Without),
  assoc(Without, To, NewToList, NewCrates).

all_movements2([], Crates, Crates) :- !.
all_movements2([Many, From, To | Rest], Crates, NewCrates) :-
  moved2(Crates, [Many, From, To], TempCrates),
  all_movements2(Rest, TempCrates, NewCrates).

make_moves2(Result) :-
  input(I),
  re_split("\n.*?\n\n"/m, I, [CratesString, _, Instructions], []),
  normalize_crates(CratesString, Crates),
  re_split("\\d+"/n, Instructions, Inst),
  include(number, Inst, NumbersOnly),
  all_movements2(NumbersOnly, Crates, Result).

second_part(Res) :-
  make_moves2(Result),
  maplist(fst, Result, Res).
