input("A Y
B X
C Z").

translate('A', rock).
translate('B', paper).
translate('C', scissors).
translate('X', rock).
translate('Y', paper).
translate('Z', scissors).
score(rock, 1).
score(paper, 2).
score(scissors, 3).

score(X, Y, 6) :-
  Wons = [[rock, scissors], [paper, rock], [scissors, paper]],
  member([X, Y], Wons).
score(X, Y, 0) :-
  Wons = [[rock, scissors], [paper, rock], [scissors, paper]],
  member([Y, X], Wons).
score(X, X, 3).

split_round(List, Round) :-
  atomic_list_concat(List, " ", Round).

score_round(Score, [O, M]) :-
  translate(O, Other),
  translate(M, Me),
  score(Me, ShapeScore),
  score(Me, Other, RoundScore),
  Score is ShapeScore + RoundScore.

score_rounds(ScoringTerm, Sum) :-
  input(Input),
  split_string(Input, "\n", "", Round),
  maplist(split_round, FullRound, Round),
  maplist(ScoringTerm, Scores, FullRound),
  sum_list(Scores, Sum).

first_part(Sum) :- score_rounds(score_round, Sum).

translate_choice('X', 0).
translate_choice('Y', 3).
translate_choice('Z', 6).

score_part_two(Score, [O, Outcome]) :-
  translate(O, Other),
  translate_choice(Outcome, RoundScore),
  score(Me, Other, RoundScore),
  score(Me, ShapeScore),
  Score is ShapeScore + RoundScore.

second_part(Sum) :- score_rounds(score_part_two, Sum).
