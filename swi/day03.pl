input("vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw").

value_of(Char, Value) :-
  String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
  string_chars(String, Chars),
  nth1(Value, Chars, Char).

first_part_vals(Value) :-
  input(I),
  split_string(I, "\n", "", Lines),
  member(Line, Lines),
  string_length(Line, Size),
  Mid is Size / 2,
  string_concat(First, Second, Line),
  string_length(First, Mid),
  string_chars(First, FirstChars1),
  list_to_set(FirstChars1, FirstChars),
  string_chars(Second, SecondChars1),
  list_to_set(SecondChars1, SecondChars),
  member(Char, SecondChars),
  member(Char, FirstChars),
  value_of(Char, Value).

first_part(Value) :-
  bagof(V, first_part_vals(V), Vs),
  sum_list(Vs, Value).

all_present([S1, S2, S3 | _], Badge) :-
  string_chars(S1, L1), list_to_set(L1, Set1), member(Badge, Set1),
  string_chars(S2, L2), list_to_set(L2, Set2), member(Badge, Set2),
  string_chars(S3, L3), list_to_set(L3, Set3), member(Badge, Set3).

all_present([_, _, _ | Rest], Badge) :-
  all_present(Rest, Badge).

second_part_vals(Value) :-
  input(I),
  split_string(I, "\n", "", Lines),
  all_present(Lines, Badge),
  value_of(Badge, Value).

second_part(Value) :-
  bagof(V, second_part_vals(V), Vs),
  sum_list(Vs, Value).
