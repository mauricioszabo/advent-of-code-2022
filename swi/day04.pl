input("2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8").

regions(Min1, Min2, Max1, Max2) :-
  input(I),
    split_string(I, "\n", "", Sections),
    member(Section, Sections),
    split_string(Section, ",", "", [Elf1, Elf2]),
    split_string(Elf1, "-", "", [Elf1Min, Elf1Max]),
    split_string(Elf2, "-", "", [Elf2Min, Elf2Max]),
    number_string(Min1, Elf1Min), number_string(Max1, Elf1Max),
    number_string(Min2, Elf2Min), number_string(Max2, Elf2Max).

fully_overlap(Min1, Min2, Max1, Max2) :- Min1 =< Min2, Max1 >= Max2, !.
fully_overlap(Min1, Min2, Max1, Max2) :- Min2 =< Min1, Max2 >= Max1, !.

first_part_vals :-
  regions(Min1, Min2, Max1, Max2),
  fully_overlap(Min1, Min2, Max1, Max2).

first_part(Many) :-
  bagof(_, first_part_vals, Vs),
  length(Vs, Many).

overlap(Min1, Min2, Max1, _) :- Min1 =< Min2, Min2 =< Max1, !.
overlap(Min1, Min2, _, Max2) :- Min2 =< Min1, Min1 =< Max2, !.

second_part_vals :-
  regions(Min1, Min2, Max1, Max2),
  overlap(Min1, Min2, Max1, Max2).

second_part(Many) :-
  bagof(_, second_part_vals, Vs),
  length(Vs, Many).
