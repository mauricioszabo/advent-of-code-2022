% :- use_module(library(pcre)).
% :- use_module(library(clpfd)).
%
input(I) :- read_file_to_string("swi/day06.txt", I, []).

take(0, _, []).
take(N, [F | Rest], Result) :-
  NewN is N - 1,
  take(NewN, Rest, RestResult),
  Result = [F | RestResult], !.

all_different(Many, List, Count, Count) :-
  take(Many, List, SubList),
  list_to_set(SubList, SubList).

all_different(Many, List, SoFar, Count) :-
  take(Many, List, SubList),
  list_to_set(SubList, Set),
  SubList \= Set,
  NewSoFar is SoFar + 1,
  [_ | Rest] = List,
  all_different(Many, Rest, NewSoFar, Count), !.

first_part(A) :-
  input(I),
  string_chars(I, Entries),
  all_different(4, Entries, 4, A).

second_part(A) :-
  input(I),
  string_chars(I, Entries),
  all_different(14, Entries, 14, A).
