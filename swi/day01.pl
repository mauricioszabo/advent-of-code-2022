input("1000
2000
3000

4000

5000
6000

7000
8000
9000

10000").

% elf_calories(CaloriesOfElf, Sum) :-
calories_of(Calories, TextBlock) :-
  split_string(TextBlock, "\n", "", CaloriesAsStr),
  maplist(number_string, CaloriesList, CaloriesAsStr),
  sum_list(CaloriesList, Calories).

sorted_calories(Input, Sorted) :-
  atomic_list_concat(ListOfBlocks, "\n\n", Input),
  maplist(calories_of, CaloriesList, ListOfBlocks),
  sort(0, @>=, CaloriesList, Sorted).

first_part :-
  input(S),
  sorted_calories(S, [Max | _]),
  print(Max), nl.

second_part :-
  input(S),
  sorted_calories(S, [A, B, C | _]),
  Max is A + B + C,
  print(Max), nl.

:- first_part, second_part, halt.
